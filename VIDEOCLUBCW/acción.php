
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Peliculas</title>
    <meta charset="UTF-8">
    <link rel="Shortcut Icon" type="image/x-icon" href="assets/icons/cw.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!--Sweealert-->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    <script src="js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="css/sweet-alert.css">
    <link rel="stylesheet"  ef="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/main.js"></script>
</head>


<style>

    .contenedor{
    position:relative;
    width:600px;
    height:390px;
    padding-left: 20px;
    padding-top: 30px;
    background-color:#343437;
    border-radius: 50px;
    margin:auto;
}


</style>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers" style="background-color:#222426;">
        	<br>
        	<br>
        	
            <div class="full-reset" style="background-color:#2F74FE;">
                <figure>
                    <img src="assets/img/VideoClub.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
                <p class="text-center" style="padding-top: 15px;"><font size="4" face="system-ui">Peliculas</font></p>
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="inicio.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>
                
                 
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">

    	 <div class="footer-copyright full-reset all-tittles" style="background-color:#2F74FE;">Video Club Cw</div>

        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><center><font size="10" face="system-ui">NUEVOS LANZAMIENTOS</font></center></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">

            </ul>
        </div>
        <div class="container-fluid">



<section id="seccion" class="col-md-8">
            <div class="row">
      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/halloween.jpg" data-holder-rendered="true" style="height: 200px; width: 200%; display: block;">
          <div class="caption">
            <h3>Halloween</h3>
            <h4>Sinopsis</h4>
            <p>Se centra en el asesino psicópata Michael Myers, quien tras pasar quince años en un hospital psiquiátrico por haber matado a su hermana mayor, escapa y reincide en sus crímenes. Esta serie creó o reinventó el género slasher.[cita requerida] Consta de once películas, con la undécima película producida por el director de la original, John Carpenter.</p>
            <hr>  
            <p><a onclick="EventoAlert1()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert1(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO UNITARIO $15.000, TIPO: TERROR, GENERO: ADULTOS, FECHA ESTRENO: 10 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
            <br>
            <br>
          </div>
        </div>
      </div>



      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/piegrande.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>Pie pequeño</h3>
            <h4>Sinopsis</h4>
            <p>Smallfoot es una película estadounidense musical de comedia y aventura animada por computadora producida por Warner Animation Group. La trama sigue a un grupo de Yetis que se encuentran con un humano cantante llamado Percy y con ambas especies pensando que la otra es solo un mito.</p>
            <a onclick="EventoAlert2()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
            <br>
    	    <div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert2(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO UNITARIO $25.000, TIPO: ANIMADO, GENERO: INFANTIL, FECHA ESTRENO: 09 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>



      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/reloj.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>La casa con un reloj en sus paredes</h3>
            <h4>Sinopsis</h4>
            <p>The House with a Clock in Its Walls es una película estadounidense de comedia-terror y fantasía gótica dirigida por Eli Roth y escrita por Eric Kripke. Está basada en la novela del mismo nombre escrita por John Bellairs e ilustrada por Edward Gorey.</p>
            <a onclick="EventoAlert3()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <div class="container-fluid h-100"> 
              <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
      </div>
           
					<!--Sweetalert -->

					<script>
				 		function EventoAlert3(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO UNITARIO $35.000, TIPO: TERROR, GENERO: ADULTOS, FECHA ESTRENO: 08 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>


        <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/buscando.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>Buscando...</h3>
            <h4>Sinopsis</h4>
            <p>Searching es una película estadounidense de suspenso psicológico dirigida por Aneesh Chaganty y protagonizada por John Cho y Debra Messing. La película está filmada desde el punto de vista de pantallas de celulares y computadoras, y sigue la historia de un padre intentando encontrar a su hija de 16 años perdida.</p>
            <a onclick="EventoAlert4()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
           <!--Sweetalert -->
 
           <script>
              function EventoAlert4(){
             swal({
                 title: "VIDEO CLUB CW",
                 text: "PRECIO UNITARIO $4.000, TIPO: SUSPENSO, GENERO: ADULTOS, FECHA ESTRENO: 01 FEBRERO 2023",
                 confirmButtonColor: "#52BE80",        
                 closeOnConfirm: false           
             })
             
             }
            </script>
          </div>
        </div>
      </div>


        </section>  
    </div>


	<!--====== Scripts -->
	
    <script>
	  // configuración inicial del carrito 
	  paypal.minicart.render({
	  strings:{
	    button:'Pagar'
	   ,buttonAlt: "Total"
	   ,subtotal: 'Total:'
	   ,empty: 'No hay productos en el carrito'
	  }
	  });
	  // Eventos para agregar productos al carrito
	  
	   $('.producto').click(function(e){
	       e.stopPropagation();
		    paypal.minicart.cart.add({
			business: 'juandavid@hotmail.com', // Cuenta paypal para recibir el dinero
			item_name: $(this).attr("titulo"),
			 amount: $(this).attr("precio"),
			 currency_code: 'COP',
			
			});
	   });
	  
	</script>

</body>
</html>