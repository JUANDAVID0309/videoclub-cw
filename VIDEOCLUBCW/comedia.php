
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Peliculas</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="assets/icons/cw.ico" />
    <script src="js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="css/sweet-alert.css">
    <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/main.js"></script>
</head>


<style>

    .contenedor{
    position:relative;
    width:600px;
    height:390px;
    padding-left: 20px;
    padding-top: 30px;
    background-color:#343437;
    border-radius: 50px;
    margin:auto;
}


</style>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers" style="background-color:#222426;">
        	<br>
        	<br>
        	
            <div class="full-reset" style="background-color:#2F74FE;">
                <figure>
                    <img src="assets/img/VideoClub.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
                <p class="text-center" style="padding-top: 15px;"><font size="4" face="system-ui">Peliculas</font></p>
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="inicio.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>
            
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">

    	 <div class="footer-copyright full-reset all-tittles" style="background-color:#2F74FE;">Video Club Cw</div>

        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><center><font size="10" face="system-ui">PELICULAS VIEJAS</font></center></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">

            </ul>
        </div>
        <div class="container-fluid">

        <section id="seccion" class="col-md-8">
            <div class="row">
      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/movie1.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>El Espanta Tiburones</h3>
            <h4>Sinopsis</h4>
            <p>El espantatiburones (título original en inglés: Shark Tale) es una película de animación de 2004 producida por DreamWorks Animation. Cuenta con las voces de Will Smith, Jack Black, Renée Zellweger, Angelina Jolie, Martin Scorsese y Robert De Niro. Su título original era Shark Slayer (en español significa El Asesina-Tiburones).</p>
            <p><a onclick="EventoAlert1()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario3.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert1(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO $10.000, TIPO: ANIMADO, GENERO: INFANTIL, FECHA ESTRENO: 14 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/movie2.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>La Familia del Futuro</h3>
            <h4>Sinopsis</h4>
            <p>La familia del futuro en Hispanoamérica) es una película de animación por computadora de Walt Disney Pictures y la producción animada número 47 de Walt Disney Animation Studios. Fue estrenada el 30 de marzo de 2007 en los Estados Unidos, siendo la segunda película de Disney Animation producida totalmente por computadora. La frase promocional es: Algunas familias son todo un viaje.</p>
            <p><a onclick="EventoAlert2()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario3.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert2(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO $10.000, TIPO: ANIMADO, GENERO: INFANTIL, FECHA ESTRENO: 20 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/movie3.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>La Fabrica de Chocolates</h3>
            <h4>Sinopsis</h4>
            <p>Charlie y la fábrica de chocolate (título original en inglés: Charlie and the Chocolate Factory) es una película estrenada en el año 2005, dirigida por Tim Burton. Es la segunda adaptación cinematográfica de la novela homónima escrita por Roald Dahl en 1964, y está protagonizada por Johnny Depp como Willy Wonka y Freddie Highmore como Charlie Bucket. </p>
            <p><a onclick="EventoAlert3()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario3.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
           <!--Sweetalert -->
 
           <script>
              function EventoAlert3(){
             swal({
                 title: "VIDEO CLUB CW",
                 text: "PRECIO $10.000, TIPO:, GENERO: VARIADO, FECHA ESTRENO: 10 FEBRERO 2023",
                 confirmButtonColor: "#52BE80",        
                 closeOnConfirm: false           
             })
             
             }
            </script>
          </div>
        </div>
      </div>
        <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/movie4.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>Matilda</h3>
            <h4>Sinopsis</h4>
            <p>​ dirigida por Danny DeVito, y producida por TriStar Pictures.3​ Basada en la novela homónima de Roald Dahl,4​ la película está protagonizada por Mara Wilson y trata sobre una niña genio llamada Matilda Wormwood, quien desarrolla habilidades psicoquinéticas y las usa para tratar con su irresponsable familia y su malvada directora.5​</p>
            <p><a onclick="EventoAlert4()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario3.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert4(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO $10.000, TIPO: VARIADO, GENERO: ADULTOS, FECHA ESTRENO: 10 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>


        </section>
    </div>

    
        
    </div>
</body>
</html>