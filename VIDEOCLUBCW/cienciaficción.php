
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Peliculas</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="assets/icons/cw.ico" />
    <script src="js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="css/sweet-alert.css">
    <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/main.js"></script>
</head>


<style>

    .contenedor{
    position:relative;
    width:600px;
    height:390px;
    padding-left: 20px;
    padding-top: 30px;
    background-color:#343437;
    border-radius: 50px;
    margin:auto;
}


</style>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
        	<br>
        	<br>
        
            <div class="full-reset" style="background-color:#2F74FE;">
                <figure>
                    <img src="assets/img/VideoClub.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
                <p class="text-center" style="padding-top: 15px;"><font size="4" face="system-ui">Peliculas</font></p>
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li>
                        <a href="inicio.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>
                    
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers" >

    	 <div class="footer-copyright full-reset all-tittles" style="background-color:#2F74FE;">Video Club Cw</div>

        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><center><font size="10" face="system-ui">PELICULAS NORMALES</font></center></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">

            </ul>
        </div>
        <div class="container-fluid">

        <section id="seccion" class="col-md-8">
            <div class="row">
      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/zombie.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>Zombie</h3>
            <h4>Sinopsis</h4>
            <p>es una película de terror estadounidense de 2004 dirigida por Zack Snyder y escrita por James Gunn. La cinta está basada en la película homónima de 1978, la cual fue creada por George A. Romero. Al igual que su antecesora, la historia se centra en un grupo de personas que se refugia en un centro comercial ante una plaga de zombis. Esta versión posee algunas variaciones en la trama y personajes. Otro de los cambios tiene que ver con los movimientos de los zombis, siendo estos más rápidos en esta versión.</p>
            <p><a onclick="EventoAlert1()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario2.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert1(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO $15.000, TIPO: TERROR, GENERO: ADULTOS, FECHA ESTRENO: 03 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/cumbre.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>La cumbre Escarlata</h3>
            <h4>Sinopsis</h4>
            <p>La cumbre escarlata (en inglés: Crimson Peak) es una película estadounidense-mexicana-canadiense de fantasía y suspenso estrenada en 2015, dirigida por Guillermo del Toro y escrita por Del Toro y Matthew Robbins. La cinta está protagonizada por Mia Wasikowska, Tom Hiddleston, Jessica Chastain, Charlie Hunnam y Jim Beaver. Fue producida por Legendary Pictures2​ y distribuida por Universal Pictures3​ y su estreno se produjo el 16 de octubre de 2015.</p>
            <p><a onclick="EventoAlert2()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario2.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert2(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO $15.000, TIPO: TERROR, GENERO: ADULTOS, FECHA ESTRENO: 16 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/nemo.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>Nemo</h3>
            <h4>Sinopsis</h4>
            <p>Buscando a Nemo (título original en inglés, Finding Nemo) es una película infantil de animación producida por Pixar Animation Studios y ganadora de un premio Óscar. Nemo es el protagonista de la película El cual tenía una aleta atrófica y por eso una era más pequeña que la otra. La historia se basa en su padre llamado Marlin un pez payaso, quien va en busca de su pequeño hijo Nemo junto a una pez cirujano llamada Dory que tiene problemas de memoria.</p>
            <p><a onclick="EventoAlert3()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario2.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert3(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO $15.000, TIPO: ANIMADA, GENERO: INFANTIL, FECHA ESTRENO: 10 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>
        <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail">
          <img data-src="holder.js/100%x200" alt="100%x200" src="assets/img/scream.jpg" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">
          <div class="caption">
            <h3>Scream</h3>
            <h4>Sinopsis</h4>
            <p>Scream es una serie de películas de terror satírico creada por Kevin Williamson y Wes Craven. Los protagonistas son Neve Campbell, Courteney Cox, y David Arquette. La serie ha recaudado más de 900 millones alrededor del mundo por sus cinco películas.[cita requerida] La primera entrega de Scream fue estrenada el 20 de diciembre de 1996 y es actualmente la película slasher que más ha recaudado en los Estados Unidos. </p>
            <p><a onclick="EventoAlert4()" class="btn btn-primary "  role="button">INFORMACIÓN</a>
            <br>
    	<div class="row w-100 align-items-center">
    			      <div class="col text-center">
                    <p><a href="preciounitario2.php" class="btn btn-warning" role="button">Precios Unitario</a> 
        			  </div>	
    		  </div>
					<!--Sweetalert -->

					<script>
				 		function EventoAlert4(){
						swal({
							  title: "VIDEO CLUB CW",
							  text: "PRECIO $15.000, TIPO: TERROR, GENERO: ADULTOS, FECHA ESTRENO: 10 FEBRERO 2023",
							  confirmButtonColor: "#52BE80",        
							  closeOnConfirm: false           
            })
            
						}
				 	</script>
          </div>
        </div>
      </div>


        </section>


        

        </div>

    </div>


    <script>
	  // configuración inicial del carrito 
	  paypal.minicart.render({
	  strings:{
	    button:'Pagar'
	   ,buttonAlt: "Total"
	   ,subtotal: 'Total:'
	   ,empty: 'No hay productos en el carrito'
	  }
	  });
	  // Eventos para agregar productos al carrito
	  
	   $('.producto').click(function(e){
	       e.stopPropagation();
		    paypal.minicart.cart.add({
			business: 'juandavid@hotmail.com', // Cuenta paypal para recibir el dinero
			item_name: $(this).attr("titulo"),
			 amount: $(this).attr("precio"),
			 currency_code: 'COP',
			
			});
	   });
	  
	</script>
	


</body>
</html>