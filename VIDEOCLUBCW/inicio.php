
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Video Club CW</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="assets/icons/cw.ico" />
    <script src="js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="css/sweet-alert.css">
    <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.css">
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
    <div class="navbar-lateral full-reset" >
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers" style="background-color:#222426;">
        <br>
        <br>
            <div class="full-reset"  style="background-color:#2F74FE;">
                <figure>
                    <img src="assets/img/VideoClub.png" alt="Biblioteca" class="img-responsive center-box" style="width:70%;">
                </figure>
            </div>
            <div class="full-reset nav-lateral-list-menu" >
                <ul class="list-unstyled">
                    <li><a href="inicio.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-movie"></i>&nbsp;&nbsp; Tipos de Peliculas <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                           <li><a href="acción.php"><i class="zmdi zmdi-movie-alt"></i>&nbsp;&nbsp; Nuevos Lanzamientos</a></li>
                            <li><a href="cienciaficción.php"><i class="zmdi zmdi-movie-alt"></i>&nbsp;&nbsp;Películas Normales </a></li>
                            <li><a href="comedia.php"><i class="zmdi zmdi-movie-alt"></i>&nbsp;&nbsp; Peliculas Viejas </a></li>
                         
                        </ul>
                    </li>
                    	 <!--<li><a href="alquiler.php"><i class="zmdi zmdi-star-outline"></i>&nbsp;&nbsp;  Alquiler de Peliculas</a></li>-->

                    	 <li><a href="desconectar.php"><i class="zmdi zmdi-power"></i>&nbsp;&nbsp; Cerrar Sesión</a></li>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">

    	 <div class="footer-copyright full-reset all-tittles" style="background-color:#2F74FE;">Video Club Cw</div>

        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><center><font size="10" face="system-ui">INVENTARIO DE PELICULAS</font></center></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">

            </ul>
        </div>
        <div class="container-fluid"  style="margin: 50px 0;">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <img src="assets/img/cine.png" alt="user" class="img-responsive center-box" style="max-width: 110px;">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    <p><strong> BIENVENIDO A VIDEO CLUB CW,PELICULAS DISPONIBLES.</strong></p>
                    
                </div>
            </div>
        </div>


<div class="container-fluid">
    <div class="container-flat-form">
        <br>
        <br>
            <center><h1>TERROR</h1></center>
            <center><h1>PRECIO UNITARIO $35.000</h1></center>    
                <a href="espera.php">
                <img src="assets/img/terror.jpg" alt="Biblioteca" class="img-responsive center-box" style="width:70%;">
                </a>
            <br>
            <br>

    <div class="container-fluid h-100"> 
    		<div class="row w-100 align-items-center">
    			<div class="col text-center">
                    <p><a href="alquiler.php" class="btn btn-primary" role="button">Calcular precios alquileres</a> 

    			</div>	
    		</div>
      </div>
       
</div>



<div class="container-flat-form">
<br>
<br>
<center><h1>ACCIÓN</h1></center>
<center><h1>PRECIO UNITARIO $15.000</h1></center> 
<a href="espera.php">
<img src="assets/img/accion.jpg" alt="Biblioteca" class="img-responsive center-box" style="width:70%;">
</a>
<br>
<br>
<div class="container-fluid h-100"> 
    		<div class="row w-100 align-items-center">
    			<div class="col text-center">
                    <p><a href="alquiler.php" class="btn btn-primary" role="button">Calcular precios alquileres</a> 

    			</div>	
    		</div>
      </div>
</div>

<div class="container-flat-form">
<br>
<br>
<center><h1>ROMANCE</h1></center>
<center><h1>PRECIO UNITARIO $18.000</h1></center> 
<a href="espera.php">
<img src="assets/img/romance.jpg" alt="Biblioteca" class="img-responsive center-box" style="width:70%;">
</a>
<br>
<br>
<div class="container-fluid h-100"> 
    		<div class="row w-100 align-items-center">
    			<div class="col text-center">
                    <p><a href="alquiler.php" class="btn btn-primary" role="button">Calcular precios alquileres</a> 

    			</div>	
    		</div>
      </div>
</div>

<div class="container-flat-form">
<br>
<br>
<center><h1>CIENCIA FICCIÓN</h1></center>
<center><h1>PRECIO UNITARIO $25.000</h1></center> 
<a href="espera.php">
<img src="assets/img/ciencia.jpg" alt="Biblioteca" class="img-responsive center-box" style="width:70%;">
</a>
<br>
<br>
<div class="container-fluid h-100"> 
    		<div class="row w-100 align-items-center">
    			<div class="col text-center">
                    <p><a href="alquiler.php" class="btn btn-primary" role="button">Calcular precios alquileres</a> 

    			</div>	
    		</div>
      </div>
</div>

        </div>
    </div>
</body>
</html>
<script src="minicart.js"></script>
	<script>
	  // configuración inicial del carrito 
	  paypal.minicart.render({
	  strings:{
	    button:'Pagar'
	   ,buttonAlt: "Total"
	   ,subtotal: 'Total:'
	   ,empty: 'No hay productos en el carrito'
	  }
	  });
	  // Eventos para agregar productos al carrito
	  
	   $('.producto').click(function(e){
	       e.stopPropagation();
		    paypal.minicart.cart.add({
			business: 'juandavid@hotmail.com', // Cuenta paypal para recibir el dinero
			item_name: $(this).attr("titulo"),
			 amount: $(this).attr("precio"),
			 currency_code: 'COP',
			
			});
	   });
	  
	</script>