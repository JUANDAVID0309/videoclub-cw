
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Peliculas</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="assets/icons/cw.ico" />
    <script src="js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="css/sweet-alert.css">
    <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/main.js"></script>
     <!--Sweealert-->
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>


<style>

    .contenedor{
    position:relative;
    width:600px;
    height:390px;
    padding-left: 20px;
    padding-top: 30px;
    background-color:#343437;
    border-radius: 50px;
    margin:auto;
}


</style>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
        	<br>
        	<br>
        
            <div class="full-reset" style="background-color:#2F74FE;">
                <figure>
                    <img src="assets/img/VideoClub.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
                <p class="text-center" style="padding-top: 15px;"><font size="4" face="system-ui">Peliculas</font></p>
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li>
                        <a href="inicio.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>
                    
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers" >

    	 <div class="footer-copyright full-reset all-tittles" style="background-color:#2F74FE;">Video Club Cw</div>

        <div class="container">
            <div class="page-header">
              <h2 class="all-tittles"><font size="10" face="system-ui"><p>PRECIO UNITARIO POR LOS 3 PRIMEROS DÍAS <CENTER>DÍA ACIDIONAL INCREMENTO 15%</CENTER></p></font></h2>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">

            </ul>
        </div>
        <div class="container-fluid">

        <form action="preciounitario2.php" method="post" class="form-container-movie" style="text-align: center;">
           
            <h2><strong>Escoge tu pelicula favorita</strong> </h2>
                    
                    <select name="tipopelicula" id="">
                        <option value="Zombie">Zombie</option>
                            <option value="La cumbre Escarlata">La cumbre Escarlata</option>
                            <option value="Nemo">Nemo</option>
                            <option value="Scream">Scream</option>
                    </select>
                          <h2><label for="">Ingresa la cantidad de días para alquilar: </label></h2>
                            <input type="number" name="txtdias" required></input>
                           
                            <br>         
                            <br>
                <button type="submit" class="btn btn-raised btn-warning" >Calcular Valor</button>
        </form>

        <?php
       
            if($_POST){
                $tipopelicula= $_POST["tipopelicula"];
                $peliculanormal = 15000;
                $dias= $_POST["txtdias"];
                
            //evaluar
            if($dias <=3){
               $incre= $peliculanormal;
               print "<strong>Pelicula :</strong>".$tipopelicula."<br>";
               print "<strong>Precio unitario   :</strong>".$peliculanormal."<br>";

               }else{

                $incre= $peliculanormal * 0.15;

                $tot =$peliculanormal + $incre;
               
                print "<strong>Pelicula :</strong>".$tipopelicula."<br>";
                
                print "<strong>Precio unitario mas incremento del 15% :</strong>".$tot;

               }
               
                    
            
            }
        ?>

       </fieldset>


        </div>

    </div>


    <script>
	  // configuración inicial del carrito 
	  paypal.minicart.render({
	  strings:{
	    button:'Pagar'
	   ,buttonAlt: "Total"
	   ,subtotal: 'Total:'
	   ,empty: 'No hay productos en el carrito'
	  }
	  });
	  // Eventos para agregar productos al carrito
	  
	   $('.producto').click(function(e){
	       e.stopPropagation();
		    paypal.minicart.cart.add({
			business: 'juandavid@hotmail.com', // Cuenta paypal para recibir el dinero
			item_name: $(this).attr("titulo"),
			 amount: $(this).attr("precio"),
			 currency_code: 'COP',
			
			});
	   });
	  
	</script>
	


</body>
</html>